from django.db import models

class Matkul(models.Model):
    nama = models.CharField(max_length=200)
    dosen = models.CharField(max_length=200)
    sks = models.CharField(max_length=200)
    deskripsi = models.TextField(blank=False, null=False)
    semester = models.CharField(max_length=200)
    kelas = models.CharField(max_length=200, default="")