from django import forms
from .models import Matkul

class Input_Form(forms.ModelForm):
    class Meta:
        model = Matkul
        fields = ['nama', 'dosen', 'sks', 'deskripsi', 'semester', 'kelas']

    error_messages = {
        'required' : 'Please Type'
    }
    input_attrs = {
        'type' : 'text',
        'class' : 'form-control'
    }
    nama = forms.CharField(label='', required=True, widget=forms.TextInput(attrs=input_attrs))
    dosen = forms.CharField(label='', required=True, widget=forms.TextInput(attrs=input_attrs))
    sks = forms.CharField(label='', required=True, widget=forms.TextInput(attrs=input_attrs))
    deskripsi = forms.CharField(label='', required=True, widget=forms.Textarea(attrs=input_attrs))
    semester = forms.CharField(label='', required=True, widget=forms.TextInput(attrs=input_attrs))
    kelas = forms.CharField(label='', required=True, widget=forms.TextInput(attrs=input_attrs))