from django.shortcuts import render
from django.http import HttpResponseRedirect
from .forms import Input_Form
from .models import Matkul

response = {}

def daftar(request):
    response = {'input_form' : Input_Form}
    return render(request, "daftarmatkul.html",response)
def detail(request, id):
    matkul = Matkul.objects.get(id=id)
    response = {'matkul' : matkul}
    return render(request, "detailmatkul.html", response)
def save(request):
    form = Input_Form(request.POST or None)
    if (form.is_valid and request.method == 'POST'):
        form.save()
        return HttpResponseRedirect('/story5/read')
    else:
        return HttpResponseRedirect('/story5/')
def read(request):
    response = {'input_form' : Input_Form}
    matkuls = Matkul.objects.all()
    response['matkuls'] = matkuls
    html = 'daftarmatkul.html'
    return render(request, html, response)
def delete(request, id):
    matkul = Matkul.objects.get(id=id)
    if request.method == "POST":
        matkul.delete()
        return HttpResponseRedirect('/story5/read')
    response = {'matkul' : matkul}
    return render(request, "detailmatkul.html", response)
