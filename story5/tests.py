from django.test import TestCase, Client
from django.urls import resolve
from .views import daftar, detail, save, read, delete
from .models import Matkul
from django.contrib.admin.sites import AdminSite
from .admin import Matkul

from story5.forms import Input_Form

class Testing5(TestCase):
    #Test Model
    def setUp(self):
        self.matkul = Matkul.objects.create(
            nama="SDAASIK",
            dosen="Izuri",
            sks="8",
            deskripsi="Izuri ga bermodal bisanya nyuruh2 doang",
            semester="Gasal 2020/2021",
            kelas="keranjang boba ajaib"
            )
    def test_model_matkul_ada(self):
        counter_matkul = Matkul.objects.all().count()
        self.assertEquals(counter_matkul, 1)
    
    #Test URL
    def test_url_story5_ada(self):
        response = Client().get('/story5/')
        self.assertEquals(response.status_code, 200)
    def test_url_save_ada(self):
        response = Client().get('/story5/save')
        self.assertEquals(response.status_code, 302) 
    def test_url_read_ada(self):
        response = Client().get('/story5/read')
        self.assertEquals(response.status_code, 200)
    def test_url_detail_ada(self):
        response = Client().get('/story5/detail/1')
        self.assertEquals(response.status_code, 200)
    def test_url_delete_ada(self):
        response = Client().get('/story5/delete/1')
        self.assertEquals(response.status_code, 200)

    #Test Template
    #def test_template_story5(self):
    #    response = Client().get('/story5/')
    #    self.assertTemplateUsed(response, 'daftarmatkul.html')
    #def test_ada_text_daftar_kegiatan(self):
    #    response = Client().get('/story5/')
    #    html_kembalian = response.content.decode('utf8')
    #    self.assertIn("Daftar Matkul", html_kembalian)

    #Test Views
    def test_ada_daftar(self):
        found = resolve('/story5/')
        self.assertEquals(found.func, daftar)
    def test_ada_save(self):
        found = resolve('/story5/save')
        self.assertEquals(found.func, save)
    def test_ada_read(self):
        found = resolve('/story5/read')
        self.assertEquals(found.func, read)
    def test_ada_detail(self):
        found = resolve('/story5/detail/1')
        self.assertEquals(found.func, detail)
    def test_ada_delete(self):
        found = resolve('/story5/delete/1')
        self.assertEquals(found.func, delete)
    def test_form_valid(self):
        form = Input_Form(data= {
            'nama' : 'genshin',
            'dosen' : 'kaeya',
            'sks' : '6',
            'deskripsi' : 'pingin main',
            'semester' : 'Gasal 2020/2021',
            'kelas' : 'mondstadt'
        })
        self.assertTrue(form.is_valid())
        