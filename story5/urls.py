from django.urls import path

from . import views

app_name = 'story5'

urlpatterns = [
    path('', views.daftar, name='daftar'),
    path('save', views.save),
    path('read', views.read, name='read'),
    path('detail/<str:id>', views.detail, name='detail'),
    path('delete/<str:id>', views.delete, name='delete')
]
