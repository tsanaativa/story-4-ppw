from django.test import TestCase, Client
from django.urls import resolve
from .views import profile

class Testing1(TestCase):
    #Test URL
    def test_url_story1_ada(self):
        response = Client().get('/story1/')
        self.assertEquals(response.status_code, 200)

    #Test Views
    def test_ada_profile(self):
        found = resolve('/story1/')
        self.assertEquals(found.func, profile)
    