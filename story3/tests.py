from django.test import TestCase, Client
from django.urls import resolve
from .views import aboutme, myexperience

class Testing3(TestCase):
    #Test URL
    def test_url_aboutme_ada(self):
        response = Client().get('/story3/aboutme/')
        self.assertEquals(response.status_code, 200)
    def test_url_myexperience_ada(self):
        response = Client().get('/story3/myexperience/')
        self.assertEquals(response.status_code, 200) 

    #Test Views
    def test_ada_aboutme(self):
        found = resolve('/story3/aboutme/')
        self.assertEquals(found.func, aboutme)
    def test_ada_myexperience(self):
        found = resolve('/story3/myexperience/')
        self.assertEquals(found.func, myexperience)
    