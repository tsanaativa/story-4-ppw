from django.urls import path

from . import views

app_name = 'story3'

urlpatterns = [
    path('aboutme/', views.aboutme, name='aboutme'),
    path('myexperience/', views.myexperience, name='myexperience'),
]
