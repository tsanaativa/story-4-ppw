from django.test import TestCase, Client
from django.urls import resolve
from .views import caribuku, daftarbuku

class Testing1(TestCase):
    #Test URL
    def test_url_story8_ada(self):
        response = Client().get('/story8/')
        self.assertEquals(response.status_code, 200)
        html_kembalian = response.content.decode('utf8')
        self.assertIn("Katalog Buku", html_kembalian)
        self.assertIn("Masukkan keyword terlebih dahulu", html_kembalian)
    def test_url_data_ada(self):
        response = Client().get('/story8/data/' + "?q=test")
        self.assertEquals(response.status_code, 200)

    #Test Views
    def test_ada_caribuku(self):
        found = resolve('/story8/')
        self.assertEquals(found.func, caribuku)
    def test_ada_daftarbuku(self):
        found = resolve('/story8/data/')
        self.assertEquals(found.func, daftarbuku)
    def test_daftarbuku_berfungsi(self):
        response = Client().get('/story8/data/' + "?q=test")
        html_kembalian = response.content.decode('utf8')
        self.assertIn("items", html_kembalian)