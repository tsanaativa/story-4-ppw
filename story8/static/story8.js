$(document).ready(function(){
    $("#input").keyup(function() {
        var keyword = $("#input").val();
        $.ajax({
            url: '/story8/data?q=' + keyword,
            success: function(data) {
                var array_books = data.items;
                $("#daftarbuku > tbody").empty();
                if (typeof array_books == 'undefined') {
                    if (keyword == '') {
                        $("#daftarbuku > tbody").append('<td colspan="4" style="text-align: center;">Masukkan keyword terlebih dahulu</td>');
                    } else {
                        $("#daftarbuku > tbody").append('<td colspan="4" style="text-align: center;">Tidak ada buku yang ditemukan dengan keyword "'+ keyword + '"</td>');
                    }
                } else {
                    for (i = 0; i < array_books.length; i++) {
                        var nomor = i + 1;
                        var judul = array_books[i].volumeInfo.title;
                        if (typeof array_books[i].volumeInfo.authors != 'undefined') {
                            var penulis = array_books[i].volumeInfo.authors.join(", ");
                        } else {
                            var penulis = "-";
                        }
                        if (typeof array_books[i].volumeInfo.imageLinks == 'undefined') {
                        var gambar = '"http://dinarpus.brebeskab.go.id/media/img/pages/no-image-available-png-3.png" style="width:100px;"';
                        } else {
                            var gambar = array_books[i].volumeInfo.imageLinks.smallThumbnail;
                        }
                        $("#daftarbuku > tbody").append("<tr><td>" + nomor + ".</td><td>" + judul + "</td><td>" + penulis + "</td><td><img src=" + gambar + "></td>")
                    }
                }
            }
        });
    });
});