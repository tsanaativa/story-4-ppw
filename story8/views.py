from django.shortcuts import render
from django.http import JsonResponse
import requests
import json

def caribuku (request):
    return render(request, 'daftarbuku.html')

def daftarbuku(request):
    url = "https://www.googleapis.com/books/v1/volumes?q=" + request.GET['q']
    ret = requests.get(url)
    data = json.loads(ret.content)
    return JsonResponse(data)