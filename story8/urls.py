from django.urls import path

from . import views

app_name = 'story8'

urlpatterns = [
    path('', views.caribuku, name='caribuku'),
    path('data/', views.daftarbuku, name='daftarbuku'),
]