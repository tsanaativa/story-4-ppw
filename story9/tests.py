from django.test import TestCase, Client
from django.urls import resolve
from .views import register
from django.contrib.auth.models import User

class Testing9(TestCase):
    def setUp(self):
        User.objects.create_user('username', password='password')

    #Test URL
    def test_url_register_ada(self):
        response = Client().get('/story9/register/')
        self.assertEquals(response.status_code, 200)
    def test_url_login_ada(self):
        response = Client().get('/story9/login/')
        self.assertEquals(response.status_code, 200)
    def test_url_logout_ada(self):
        response = Client().get('/story9/logout/')
        self.assertEquals(response.status_code, 200)
    def test_url_welcome_ada(self):
        response = Client().get('/story9/')
        self.assertEquals(response.status_code, 200)

    #Test Views
    def test_fungsi_path_register(self):
        found = resolve('/story9/register/')
        self.assertEquals(found.func, register)
    def test_register_valid(self):
        response = Client().post('/story9/register/', {'username':'manusia', 'password1':'orang1209', 'password2':'orang1209'}, follow=True)
        html_kembalian = response.content.decode('utf8')
        self.assertIn("Akun manusia berhasil dibuat!", html_kembalian)
        self.assertEquals(response.status_code, 200)
    def test_register_tidak_valid(self):
        response = Client().post('/story9/register/', {'username':'manusia', 'password1':'123', 'password2':'123'})
        html_kembalian = response.content.decode('utf8')
        self.assertIn("Akun gagal dibuat, ulangi kembali", html_kembalian)
        self.assertEquals(response.status_code, 200)
    def test_login_valid(self):
        response = Client().post('/story9/login/', {'username':'username', 'password':'password'}, follow=True)
        html_kembalian = response.content.decode('utf8')
        self.assertIn("Selamat datang, username.", html_kembalian)
        self.assertIn("Log Out", html_kembalian)
        self.assertEquals(response.status_code, 200)
    def test_login_tidak_valid(self):
        response = Client().post('/story9/login/', {'username':'manusia', 'password':'password'})
        html_kembalian = response.content.decode('utf8')
        self.assertIn("Username:", html_kembalian)
        self.assertEquals(response.status_code, 200)
    def test_logout(self):
        response = Client().post('/story9/logout/', follow=True)
        html_kembalian = response.content.decode('utf8')
        self.assertIn("Anda berhasil logout.", html_kembalian)
        self.assertIn("Log In", html_kembalian)
        self.assertIn("Sign Up", html_kembalian)
        self.assertEquals(response.status_code, 200)
