from django.urls import path
from django.contrib.auth import views as auth_views

from . import views
from .forms import LoginForm

app_name = 'story9'

urlpatterns = [
    path('register/', views.register, name='register'),
    path('login/', auth_views.LoginView.as_view(template_name='login.html', form_class=LoginForm), name='login'),
    path('', views.welcome, name='welcome'),
    path('logout/', auth_views.LogoutView.as_view(template_name='logout.html'), name='logout'),
]