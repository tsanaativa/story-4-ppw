from django.shortcuts import render, redirect
from .forms import RegisterForm
from django.contrib import messages

def register(request):
    if request.method == 'POST':
        form = RegisterForm(request.POST)
        if form.is_valid():
            form.save()
            username = form.cleaned_data.get('username')
            messages.success(request, f'Akun {username} berhasil dibuat!')
            return redirect('story9:login')
        else:
            messages.error(request, f'Akun gagal dibuat, ulangi kembali')
    else:
        form = RegisterForm()
    return render (request, 'register.html', {'form' : form})

def welcome(request):
    return render(request, 'welcome.html')
