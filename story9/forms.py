from django import forms
from django.contrib.auth.models import User
from django.contrib.auth.forms import UserCreationForm, AuthenticationForm

class RegisterForm(UserCreationForm):
    username = forms.CharField(label="Username", widget=forms.TextInput(attrs={'class': 'form-control'}), help_text='Wajib diisi. Maksimal 150 karakter. Hanya dapat memuat huruf, angka dan @/./+/-/_.')
    password1 = forms.CharField(label = "Password", widget=forms.PasswordInput(attrs={'class': 'form-control'}), help_text="Harus terdiri dari minimal 8 karakter, dilarang hanya berisi angka.")
    password2 = forms.CharField(label= "Konfirmasi Password", widget=forms.PasswordInput(attrs={'class': 'form-control'}), help_text='Masukkan password pada kolom sebelumnya untuk verifikasi.')

    class Meta(UserCreationForm.Meta):
        model = User
        fields = ['username', 'password1', 'password2']
        labels = {
            'username':'Username',
            'password1' : 'Password',
            'password2' : 'Password Confirmation',
        }

class LoginForm(AuthenticationForm):
    username = forms.CharField(label="Username", widget=forms.TextInput(attrs={'class': 'form-control'}))
    password = forms.CharField(label="Password", widget=forms.PasswordInput(attrs={'class': 'form-control'}))

    def setUp(self):
        User.objects.create_user('username', password='password')
