from django.test import TestCase, Client
from django.urls import resolve
from .views import accordion

class Testing1(TestCase):
    #Test URL
    def test_url_story7_ada(self):
        response = Client().get('/story7/')
        self.assertEquals(response.status_code, 200)
        self.assertTemplateUsed(response, "accordion.html")
        self.assertContains(response, "Accordion Buatan Saya")
        self.assertContains(response, "Aktivitas Saat Ini")
        self.assertContains(response, "Pengalaman Organisasi")
        self.assertContains(response, "Pengalaman Kepanitiaan")
        self.assertContains(response, "Prestasi")
        self.assertContains(response, "Hiburan")

    #Test Views
    def test_ada_accordion(self):
        found = resolve('/story7/')
        self.assertEquals(found.func, accordion)