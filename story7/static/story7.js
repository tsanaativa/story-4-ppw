$(document).ready(function(){
    var buttonClicked = false;
    $(".accordion-header").click(function(){
        if (buttonClicked != true) {
            $(this).next(".accordion-body").slideToggle();
        } else {
            buttonClicked = false;
        }
    })
    $(".up").click(function() {
        buttonClicked = true;
        var this_li = $(this).parents("li");
        this_li.insertBefore(this_li.prev());
    })
    $(".down").click(function() {
        buttonClicked = true;
        var this_li = $(this).parents("li");
        this_li.insertAfter(this_li.next());
    })
});