from django.shortcuts import render
from django.http import HttpResponseRedirect
from .forms import Input_Kegiatan, Input_Orang
from .models import Kegiatan, Orang

response = {}


def savekegiatan(request):
    form = Input_Kegiatan(request.POST or None)
    if (form.is_valid and request.method == 'POST'):
        form.save()
    orangs = Orang.objects.all()
    kegiatans = Kegiatan.objects.all()
    response = {'input_kegiatan' : Input_Kegiatan, 'kegiatans':kegiatans, 'orangs':orangs}
    html = 'daftarkegiatan.html'
    return render(request, html, response)
def peserta(request, id):
    kegiatan = Kegiatan.objects.get(id=id)
    response = {'kegiatan' : kegiatan, 'input_orang' : Input_Orang}
    return render(request, "daftarpeserta.html", response)
def savepeserta(request, id):
    form = Input_Orang(request.POST or None)
    if (form.is_valid and request.method == 'POST'):
        kegiatan = Kegiatan.objects.get(id=id)
        Orang.objects.create(nama=request.POST['nama'],kegiatan=kegiatan)
    return HttpResponseRedirect('/story6/')
