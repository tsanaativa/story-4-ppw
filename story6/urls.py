from django.urls import path

from . import views

app_name = 'story6'

urlpatterns = [
    path('', views.savekegiatan, name='kegiatan'),
    path('<int:id>/', views.peserta, name='peserta'),
    path('<int:id>/savepeserta', views.savepeserta, name='savepeserta')
]
