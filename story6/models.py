from django.db import models

class Kegiatan(models.Model):
    nama = models.CharField(max_length=200)

    def __str__(self):
        return self.nama

class Orang(models.Model):
    nama = models.CharField(max_length=200)
    kegiatan = models.ForeignKey(Kegiatan, on_delete=models.CASCADE)

    def __str__(self):
        return self.nama