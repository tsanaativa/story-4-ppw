from django import forms
from .models import Kegiatan, Orang

class Input_Kegiatan(forms.ModelForm):
    class Meta:
        model = Kegiatan
        fields = ['nama']

    error_messages = {
        'required' : 'Please Type'
    }
    input_attrs = {
        'type' : 'text',
        'class' : 'form-control'
    }
    nama = forms.CharField(label='', required=True, widget=forms.TextInput(attrs=input_attrs))
class Input_Orang(forms.ModelForm):
    class Meta:
        model = Orang
        fields = ['nama']

    error_messages = {
        'required' : 'Please Type'
    }
    input_attrs = {
        'type' : 'text',
        'class' : 'form-control',
        'placeholder' : 'Nama saya...',
    }
    nama = forms.CharField(label='', required=True, widget=forms.TextInput(attrs=input_attrs))