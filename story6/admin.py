from django.contrib import admin
from .models import Kegiatan, Orang


admin.site.register(Kegiatan)
admin.site.register(Orang)