from django.test import TestCase, Client
from django.urls import resolve
from .views import savekegiatan, peserta, savepeserta
from .models import Kegiatan, Orang
from django.contrib.admin.sites import AdminSite
from .admin import Kegiatan, Orang

class Testing6(TestCase):
    #Test Model
    def setUp(self):
        self.kegiatan = Kegiatan.objects.create(nama="MAKAN")
        self.orang = Orang.objects.create(nama="VIVIN", kegiatan=self.kegiatan)
    def test_model_kegiatan_ada(self):
        counter_kegiatan = Kegiatan.objects.all().count()
        self.assertEquals(counter_kegiatan, 1)
    def test_model_orang_ada(self):
        counter_orang = Orang.objects.all().count()
        self.assertEquals(counter_orang, 1)
    def test_str_kegiatan(self):
        self.assertEquals(str(self.kegiatan), self.kegiatan.nama)
    def test_str_orang(self):
        self.assertEquals(str(self.orang), self.orang.nama)
    
    #Test URL
    def test_url_story6_ada(self):
        response = Client().get('/story6/')
        self.assertEquals(response.status_code, 200)
    def test_url_peserta_ada(self):
        response = Client().get('/story6/1/')
        self.assertEquals(response.status_code, 200) 
    def test_url_savepeserta_ada(self):
        response = Client().get('/story6/1/savepeserta')
        self.assertEquals(response.status_code, 302)

    #Test Template
    def test_template_story6(self):
        response = Client().get('/story6/')
        self.assertTemplateUsed(response, 'daftarkegiatan.html')
    def test_ada_text_daftar_kegiatan(self):
        response = Client().get('/story6/')
        html_kembalian = response.content.decode('utf8')
        self.assertIn("Daftar Kegiatan", html_kembalian)

    #Test Views
    def test_ada_save_kegiatan(self):
        found = resolve('/story6/')
        self.assertEquals(found.func, savekegiatan)
    def test_ada_peserta(self):
        found = resolve('/story6/1/')
        self.assertEquals(found.func, peserta)
    def test_ada_savepeserta(self):
        found = resolve('/story6/1/savepeserta')
        self.assertEquals(found.func, savepeserta)
    def test_kegiatan_telah_disimpan_dan_ditampilkan(self):
        response = Client().post('/story6/', {'nama':'Sepedaan'})
        html_kembalian = response.content.decode('utf8')
        self.assertIn("Ikuti Kegiatan Seru", html_kembalian)
        self.assertIn("Sepedaan", html_kembalian)